import collections
import os
from pathlib import Path
import shlex
import stat
import subprocess

import yaml

# Mimicking https://github.com/nss-dev/nss/blob/master/lib/sysinit/nsssysinit.c#L64
for USER_PKI_DIR in [
    Path.home() / ".pki",
    Path(os.getenv("XDG_DATA_HOME", Path.home() / ".local" / "share")) / "pki",
]:
    if USER_PKI_DIR.exists():
        break

AGE_DIR = USER_PKI_DIR / "age"
AGE_USERS = AGE_DIR / "public"

if "AGE_SECRET_KEY" in os.environ:
    AGE_SECRET_KEY = Path(os.getenv("AGE_SECRET_KEY"))
elif "IDENTITIES_FILE" in os.environ:
    AGE_SECRET_KEY = Path(os.getenv("IDENTITIES_FILE"))
else:
    AGE_SECRET_KEY = AGE_DIR / "private" / "user.key"

MINISIGN_PUBLIC_KEY = Path.home() / "minisign.pub"
MINISIGN_SECRET_KEY = Path.home() / ".minisign" / "minisign.key"
MINISIGN_USERS = USER_PKI_DIR / "minisign"


def tree():
    return collections.defaultdict(tree)


def _call_hook(script_path, args=(), **kwargs):
    if (script_path := Path(script_path)).exists():
        if script_path.stat().st_mode & stat.S_IXUSR:
            # pylint: disable=subprocess-run-check
            return subprocess.run([script_path, *args], **kwargs)


def add_users(names):
    _call_hook(AGE_USERS / "hooks" / "add-users", [AGE_USERS, *names])
    _call_hook(MINISIGN_USERS / "hooks" / "add-users", [AGE_USERS, *names])


def get_users(default=None):
    users = tree()
    # .pub encryption keys (expected to come from age) overrule .keys (expected to come from github)
    for path in [*AGE_USERS.glob("*.keys"), *AGE_USERS.glob("*.pub")]:
        user = path.stem
        users[user]["encrypt"]["file"] = path
    if default:
        users[None]["encrypt"] = default
    elif proc := _call_hook(
        AGE_USERS / "hooks" / "guess-default-recipients", stdout=subprocess.PIPE
    ):
        if proc.returncode == 0:
            users[None]["encrypt"] = yaml.safe_load(proc.stdout)
    for path in MINISIGN_USERS.glob("*.pub"):
        user = path.stem
        users[user]["verify"]["file"] = path
    return dict(users)


def ensure_identities(target=AGE_SECRET_KEY):
    if target.exists():
        return [target]

    target.parent.mkdir(parents=True, exist_ok=True)
    if proc := _call_hook(
        AGE_DIR / "hooks" / "ensure-identities", stdout=subprocess.PIPE
    ):
        if proc.returncode == 0:
            target.write_text(proc.stdout.decode().strip())
            return [target]
    return []


def get_shell_variables():
    return f"""\
USER_PKI_DIR={shlex.quote(str(USER_PKI_DIR))}
AGE_DIR={shlex.quote(str(AGE_DIR))}
AGE_USERS={shlex.quote(str(AGE_USERS))}
AGE_SECRET_KEY={shlex.quote(str(AGE_SECRET_KEY))}
MINISIGN_PUBLIC_KEY={shlex.quote(str(MINISIGN_PUBLIC_KEY))}
MINISIGN_SECRET_KEY={shlex.quote(str(MINISIGN_SECRET_KEY))}
MINISIGN_USERS={shlex.quote(str(MINISIGN_USERS))}
"""


if __name__ == "__main__":
    print(get_shell_variables())
