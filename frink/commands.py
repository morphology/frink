import hmac
from pathlib import Path
import shutil
import subprocess
import sys

import click
import keyring

from . import api
from . import backend
from . import paths


@click.group()
@click.pass_context
def frink_group(ctx):
    # assert not ctx.invoked_subcommand
    pass


@frink_group.command("keys")
@click.option("--interactive/--batch", default=sys.stdin.isatty())
def show_secret_keys(interactive):
    print(
        "Encryption key",
        paths.AGE_SECRET_KEY,
        "exists" if paths.AGE_SECRET_KEY.exists() else "not found",
    )
    if interactive:
        try:
            print(api.show_encrypt_key())
        except Exception as e:
            print("Failed:", e, file=sys.stderr)
    else:
        print("TODO: show public key without password input", file=sys.stderr)
    print()
    print(
        "Verification key",
        paths.MINISIGN_SECRET_KEY,
        "exists" if paths.AGE_SECRET_KEY.exists() else "not found",
    )
    try:
        print(api.show_verify_key())
    except Exception as e:
        print("Failed:", e, file=sys.stderr)


@frink_group.command("enc")
@click.option("--recipients", "-t", multiple=True, default=[])
@click.argument(
    "file_in",
    nargs=1,
    required=False,
    type=click.Path(readable=True, exists=True, dir_okay=False),
)
@click.option(
    "--file-out",
    "-o",
    nargs=1,
    type=click.Path(writable=True, dir_okay=False),
)
@click.option(
    "--signature-file",
    nargs=1,
    type=click.Path(writable=True, dir_okay=False),
)
@click.option("--armor/--no-armor", "-a", default=False)
def encrypt_and_sign(
    recipients, file_in=None, file_out=None, signature_file=None, armor=False
):
    if file_out is None and sys.stdout.isatty() and not armor:
        armor = True
        print("Refusing to pump binary to the terminal", file=sys.stderr)
    for path in api.encrypt_and_sign(
        recipients, file_in, file_out, signature_file=signature_file, armor=armor
    ):
        print(path)


@frink_group.command("sign")
@click.argument(
    "file_in",
    nargs=1,
    required=True,
    type=click.Path(readable=True, exists=True, dir_okay=False),
)
@click.option(
    "--signature-file",
    nargs=1,
    type=click.Path(writable=True, dir_okay=False),
)
def clearsign(file_in=None, signature_file=None):
    subprocess.run(
        backend.sign(input_files=[file_in], signature_file=signature_file), check=True
    )


@click.option("--armor/--no-armor", "-a", default=False)
@frink_group.command("self")
@click.argument(
    "file_in",
    nargs=1,
    required=False,
    type=click.Path(readable=True, exists=True, dir_okay=False),
)
@click.option(
    "--file-out",
    "-o",
    nargs=1,
    type=click.Path(writable=True, dir_okay=False),
)
@click.option(
    "--signature-file",
    nargs=1,
    type=click.Path(writable=True, dir_okay=False),
)
@click.option("--armor/--no-armor", "-a", default=False)
def self_encrypt(
    file_in=None,
    file_out=None,
    signature_file=None,
    signing_password=keyring.get_password("frink-signing-password", "<self>"),
    armor=False,
):
    if signature_file is None:
        if file_out:
            file_out = Path(file_out)
            signature_file = Path(f"{file_out}.hmac")
    subprocess.run(
        backend.default_encrypt(file_in=file_in, file_out=file_out, armor=armor),
        check=True,
    )
    if signature_file and signature_file.suffix == ".hmac":
        if signature_file.exists():
            shutil.copy(signature_file, f"{signature_file}~")
        signature_file.write_text(
            hmac.new(
                signing_password
                or click.prompt(
                    "Enter signing password: ",
                    hide_input=True,
                    confirmation_prompt=True,
                ),
                file_out.read_bytes(),
                backend.HMAC_ALGO,
            ).hexdigest()
        )


@frink_group.command("dec")
@click.option("--sender", "-f", "name")
@click.argument(
    "file_in",
    nargs=1,
    required=False,
    type=click.Path(readable=True, exists=True, dir_okay=False),
)
@click.option(
    "--file-out",
    "-o",
    nargs=1,
    type=click.Path(writable=True, dir_okay=False),
)
@click.option(
    "--signature-file",
    nargs=1,
    type=click.Path(readable=True, dir_okay=False),
)
def verify_and_decrypt(name, file_in=None, file_out=None, signature_file=None):
    if name:
        assert sys.stdin.isatty(), "minisign expects a passphrase on stdin"
    api.verify_and_decrypt(
        name,
        file_in,
        file_out,
        signature_file=signature_file,
        signature_password=None
        if name
        else click.prompt(
            "Enter signing password: ",
            hide_input=True,
        ),
    )


@frink_group.command()
@click.argument("recipients", nargs=-1)
def add(recipients):
    paths.add_users(recipients)


@frink_group.command("list")
def show_public_keys():
    for user, keys in paths.get_users().items():
        print(user or "<default>", dict(keys))


@frink_group.command("gen")
@click.option("--interactive/--batch", default=sys.stdin.isatty())
def generate_keys(interactive):
    if paths.AGE_SECRET_KEY.is_symlink():
        print(
            f"Encryption key {paths.AGE_SECRET_KEY} is symlinked to {paths.AGE_SECRET_KEY.readlink()}"
        )
    elif not paths.AGE_SECRET_KEY.exists():
        print("Generating", paths.AGE_SECRET_KEY)
        paths.AGE_SECRET_KEY.parent.mkdir(parents=True, exist_ok=True)
        api.generate_encryption_key(paths.AGE_SECRET_KEY, no_passphrase=not interactive)
    if paths.MINISIGN_SECRET_KEY.is_symlink():
        print(
            f"Verify key {paths.MINISIGN_SECRET_KEY} is symlinked to {paths.MINISIGN_SECRET_KEY.readlink()}"
        )
    elif not paths.MINISIGN_SECRET_KEY.exists():
        print("Generating", paths.MINISIGN_SECRET_KEY)
        paths.MINISIGN_SECRET_KEY.mkdir(parents=True, exist_ok=True)
        subprocess.run(
            backend.generate_signing_key(
                paths.MINISIGN_SECRET_KEY,
                paths.MINISIGN_PUBLIC_KEY,
            ),
            check=True,
        )


@frink_group.command("paths")
def show_paths():
    print(paths.get_shell_variables())


def main():
    try:
        frink_group(
            obj={}
        )  # pylint: disable=no-value-for-parameter,unexpected-keyword-arg
    except Exception as e:
        return e
