import hashlib
import platform
from pathlib import Path

from . import paths

HMAC_ALGO = hashlib.blake2s if platform.machine() == "i386" else hashlib.blake2b


def _age_command(
    *args,
    file_in=None,
    file_out=None,
    identities=(),
    recipients=(),
    no_passphrase=None,
    armor=False
):
    args = list(args)
    if no_passphrase is None:
        no_passphrase = recipients and not {"-p", "--passphrase"} & set(args)
    if armor:
        args += ["--armor"]
    for identity_file in identities:
        args += ["-i", identity_file]
    for public_key_file in recipients:
        args += ["--recipients-file", public_key_file]
    if not no_passphrase:
        if not set(args) & {"-d", "--decrypt"}:
            args += ["-p"]
    if file_out is not None:
        args += ["-o", file_out]
    if file_in is not None:
        args.append(file_in)
    return ["age", *args]


def password_protect(**kwargs):
    return _age_command("--encrypt", recipients=[], no_passphrase=False, **kwargs)


def default_encrypt(**kwargs):
    return encrypt(recipients=[None], no_passphrase=True, **kwargs)


def encrypt(recipients=(), no_passphrase=True, **kwargs):
    users = paths.get_users()
    return _age_command(
        "--encrypt",
        recipients=[
            u if u and Path(u).is_absolute() else users[u]["encrypt"]
            for u in recipients
        ],
        no_passphrase=no_passphrase,
        **kwargs
    )


def decrypt(identities=None, **kwargs):
    if identities is None:
        identities = paths.ensure_identities()
    return _age_command("--decrypt", identities=identities, **kwargs)


def sign(input_files, signature_file=None, secret_key=paths.MINISIGN_SECRET_KEY):
    args = ["-S"]
    if secret_key:
        args += ["-s", secret_key]
    if signature_file:
        args += ["-x", signature_file]
    return ["minisign", *args, "-m", *input_files]


def verify(user, path, signature_file=None):
    assert path, "minisign cannot handle stdin"
    users = paths.get_users()
    args = [
        "-V",
        "-p",
        user if user and Path(user).is_absolute() else users[user]["verify"],
    ]
    if signature_file:
        args += ["-x", signature_file]
    return ["minisign", *args, "-m", path]


def generate_signing_key(file_out=None, public_file=None):
    # symmetric encryption by minisign
    args = []
    if file_out:
        args += ["-s", file_out]
    if public_file:
        args += ["-p", public_file]
    return ["minisign", "-G"] + args
