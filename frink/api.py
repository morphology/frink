import hmac
from pathlib import Path
import subprocess
import sys

import keyring

from . import backend
from . import paths


def show_encrypt_key(no_passphrase=None, **kwargs):
    args = []
    pipe = None
    if no_passphrase:
        args.append(paths.AGE_SECRET_KEY)
    else:
        pipe = decrypt(
            file_in=paths.AGE_SECRET_KEY, identities=(), stdout=subprocess.PIPE
        ).stdout
    return (
        subprocess.run(["age-keygen", "-y", *args], check=True, input=pipe, **kwargs)
        .stdout.decode()
        .strip()
    )


def show_verify_key():
    return paths.MINISIGN_PUBLIC_KEY.read_text().strip()


def generate_encryption_key(file_out=None, no_passphrase=None):
    if no_passphrase:
        args = ["-o", file_out] if file_out else []
        return subprocess.run(["age-keygen", *args], check=True).stdout
    else:
        return password_protect(
            input=subprocess.run(
                ["age-keygen"], check=True, stdout=subprocess.PIPE
            ).stdout,
            file_out=file_out,
        )


def password_protect(file_in=None, file_out=None, armor=False, **kwargs):
    return subprocess.run(
        backend.password_protect(file_in=file_in, file_out=file_out, armor=armor),
        check=True,
        **kwargs,
    )


def decrypt(file_in=None, file_out=None, identities=(), **kwargs):
    return subprocess.run(
        backend.decrypt(
            file_in=file_in,
            file_out=file_out,
            identities=identities,
        ),
        check=True,
        **kwargs,
    )


def encrypt_and_sign(
    recipients, file_in=None, file_out=None, signature_file=None, armor=False, **kwargs
):
    subprocess.run(
        backend.encrypt(
            recipients=recipients,
            file_in=file_in,
            file_out=file_out,
            armor=armor,
        ),
        check=True,
        **kwargs,
    )
    expected_files = []
    if file_out:
        expected_files.append(file_out)
        if sys.stdin.isatty():
            print("Enter signing key", file=sys.stderr)
            subprocess.run(
                backend.sign(input_files=[file_out], signature_file=signature_file),
                stdout=sys.stderr,
                check=True,
            )
            expected_files.append(signature_file or f"{file_out}.minisig")
        else:
            print("TODO: Skipping signing", file=sys.stderr)
    return expected_files


def verify_and_decrypt(
    name,
    file_in=None,
    file_out=None,
    signature_file=None,
    signing_password=None,
    **kwargs,
):
    if name:
        subprocess.run(
            backend.verify(name, path=file_in, signature_file=signature_file),
            stdout=sys.stderr,
            check=True,
        )
    elif file_in:
        file_in = Path(file_in)
        if (
            signature_file is None
            and (maybe_signature_file := Path(f"{file_in}.hmac")).exists()
        ):
            signature_file = maybe_signature_file
        if signature_file and signature_file.suffix == ".hmac":
            assert hmac.compare_digest(
                hmac.new(
                    signing_password, file_in.read_bytes(), backend.HMAC_ALGO
                ).hexdigest(),
                signature_file.read_text().strip(),
            )
    else:
        print("TODO: Skipping signature verification")
    return subprocess.run(
        backend.decrypt(file_in=file_in, file_out=file_out), check=True, **kwargs
    )
