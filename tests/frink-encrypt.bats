load 'frink_common'

setup_file() {
  fresh_setup
}

@test "Should fail when input not found" {
  type $deps || skip "Missing dependencies"
  run $target enc does_not_exist
  assert_failure
}

@test "Should add an encryption key" {
  type $deps || skip "Missing dependencies"
  run $target add "matt-hayden"
  assert_success
  assert [ -s "${AGE_USERS}/matt-hayden.keys" ]

  run $target list
  assert_success
  assert_line --partial "matt-hayden"
}

@test "Should fail when no recipients given" {
  type $deps || skip "Missing dependencies"
  file_out=$(mktemp -t XXXXXXXX.age)
  run $target enc small_file -o "$file_out"
  assert_failure
}

@test "Should encrypt small file in binary mode" {
  type $deps || skip "Missing dependencies"
  run $target enc -t "matt-hayden" small_file
  assert_success
}

@test "Should pipe encrypt small stdin in binary mode" {
  type $deps || skip "Missing dependencies"
  file_out=$(mktemp -t XXXXXXXX.age)
  # Workaround for BATS run capturing stdout
  capture() {
    $target enc -t "matt-hayden" > "$file_out"
  }
  run capture < small_file
  assert_success
  assert [ -s "$file_out" ]
}

@test "Should encrypt small file in armored mode" {
  type $deps || skip "Missing dependencies"
  file_out=$(mktemp -t XXXXXXXX.pem)
  run $target enc -a -t "matt-hayden" small_file -o "$file_out"
  assert_success
  grep -- "-----BEGIN AGE ENCRYPTED FILE-----" "$file_out"
  grep -- "-----END AGE ENCRYPTED FILE-----" "$file_out"
}

@test "Should encrypt small stdin in armored mode" {
  type $deps || skip "Missing dependencies"
  run $target enc -a -t "matt-hayden" < small_file
  assert_success
  assert_line "-----BEGIN AGE ENCRYPTED FILE-----"
  assert_line "-----END AGE ENCRYPTED FILE-----"
}

@test "Should pipe encrypt small stdin in armored mode" {
  type $deps || skip "Missing dependencies"
  file_out=$(mktemp -t XXXXXXXX.pem)
  # Workaround for BATS run capturing stdout
  capture() {
    $target enc -a -t "matt-hayden" > "$file_out"
  }
  run capture < small_file
  assert_success
  assert [ -s "$file_out" ]
}
