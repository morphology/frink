"""

NOTE: this is interactive and will block at several password prompts
"""

from datetime import datetime
import os
import tempfile

import pytest

from mock_frink import *  # noqa: W0401,W0611


@pytest.fixture
def mock_small_file(monkeypatch):
    with tempfile.NamedTemporaryFile("w", delete=False) as tf:
        tf.writelines("\n".join([
            __file__,
            datetime.utcnow().isoformat()
        ]))
    return tf.name


def test_add_user(mock_fresh_setup):
    import frink
    frink.add_users(["matt-hayden"])
    assert "matt-hayden" in frink.paths.get_users()


@pytest.mark.interactive
def test_encrypt_and_sign(mock_fresh_setup, mock_small_file):
    import frink
    file_out = tempfile.mktemp()
    print("Encrypting and signing", mock_small_file, "into", file_out)
    frink.encrypt_and_sign(recipients=["matt-hayden"], file_in=mock_small_file, file_out=file_out)
    assert os.path.getsize(file_out) > 0


@pytest.mark.parametrize(
    "file_in",
    ["examples/small_binary_file.age", "examples/small_text_file.age"]
)
def test_verify_and_decrypt(mock_canned_setup, file_in):
    import frink
    assert "example-signing-key" in frink.paths.get_users()
    frink.verify_and_decrypt(name="example-signing-key", file_in=file_in)
