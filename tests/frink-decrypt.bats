load 'frink_common'

setup_file() {
  canned_setup
}

@test "Fails when input not found" {
  type $deps || skip "Missing dependencies"
  run $target dec does_not_exist
  assert_failure
}

@test "Should decrypt a small binary file (no signer)" {
  type $deps || skip "Missing dependencies"
  run $target dec "${examples_dir}/small_binary_file.age"
  assert_success
  assert_line --partial "Dec 12"
}

@test "Should decrypt a small armored file (no signer)" {
  type $deps || skip "Missing dependencies"
  run $target dec "${examples_dir}/small_text_file.age"
  assert_success
  assert_line --partial "Dec 12"
}