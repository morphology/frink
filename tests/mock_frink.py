# flake8: noqa

import shutil
import subprocess

import pytest

from mock_home import mock_home


@pytest.fixture
def mock_canned_setup(mock_home):
    from frink.paths import AGE_SECRET_KEY, MINISIGN_USERS
    AGE_SECRET_KEY.parent.mkdir(parents=True, exist_ok=True)
    MINISIGN_USERS.mkdir(parents=True, exist_ok=True)

    print("Replacing encryption key", AGE_SECRET_KEY)
    shutil.copy("examples/example-encryption-key", AGE_SECRET_KEY)

    print("Replacing verify keys in", MINISIGN_USERS)
    shutil.copy("examples/example-signing-key.pub", MINISIGN_USERS)


def setup_age(mock_home):
    from frink.paths import AGE_DIR, AGE_USERS, AGE_SECRET_KEY
    from frink.api import generate_encryption_key

    AGE_SECRET_KEY.parent.mkdir(parents=True, exist_ok=True)
    generate_encryption_key(AGE_SECRET_KEY, no_passphrase=False)

    (AGE_USERS / "hooks").mkdir(parents=True, exist_ok=True)
    shutil.copy("examples/hooks/add-users_GitHub", AGE_USERS / "hooks")
    shutil.copy("examples/hooks/guess-default-recipients", AGE_USERS / "hooks")

    (AGE_DIR / "hooks").mkdir(parents=True, exist_ok=True)
    shutil.copy("examples/hooks/ensure-identities", AGE_DIR / "hooks")


def setup_minisign(mock_home):
    from frink.paths import MINISIGN_PUBLIC_KEY, MINISIGN_SECRET_KEY, MINISIGN_USERS
    from frink.backend import generate_signing_key

    if not MINISIGN_SECRET_KEY.exists():
        print("Generating signing key...")
        subprocess.run(
            generate_signing_key(
                MINISIGN_SECRET_KEY,
                MINISIGN_PUBLIC_KEY,
            ),
            check=True,
        )
    MINISIGN_USERS.mkdir(parents=True, exist_ok=True)


@pytest.fixture
def mock_fresh_setup(mock_home):
    print("Output from mocking homedir for age")
    setup_age(mock_home)
    print("Output from mocking homedir for minisign")
    setup_minisign(mock_home)
