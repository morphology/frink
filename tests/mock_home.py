from pathlib import Path
import tempfile

import pytest

MOCK_HOME = Path(tempfile.mkdtemp())


@pytest.fixture
def mock_home(monkeypatch):
    print("Using", MOCK_HOME)
    (MOCK_HOME / ".pki" / "age" / "private").mkdir(parents=True, exist_ok=True)
    (MOCK_HOME / ".local" / "share" / "pki" / "age" / "private").mkdir(parents=True, exist_ok=True)
    monkeypatch.setattr(Path, "home", lambda: MOCK_HOME)
    monkeypatch.delenv("SSH_AUTH_SOCK")
    monkeypatch.delenv("XDG_DATA_HOME")
