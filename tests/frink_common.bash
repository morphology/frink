load 'lib/bats-support/load'
load 'lib/bats-assert/load'

export examples_dir=$(realpath examples/)
deps="age minisign"
target="python -m frink"


fresh_setup() {
  export HOME=$(TMPDIR=${BATS_FILE_TMPDIR} mktemp -d)
  export XDG_DATA_HOME="$HOME/.local/share"
  eval $( $target paths )

  export AGE_USERS  # for "Should add an encryption key"

  if ! [[ -e "$AGE_SECRET_KEY" ]]; then
    mkdir -p $(dirname "$AGE_SECRET_KEY")
    echo "Generating encryption key in $AGE_SECRET_KEY"
    age-keygen -o "$AGE_SECRET_KEY"
  fi

  if ! [[ -e "$MINISIGN_SECRET_KEY" ]]; then
    mkdir -p $(dirname "$MINISIGN_SECRET_KEY")
    echo "Generating verify key in $MINISIGN_SECRET_KEY"
    minisign -G -s "$MINISIGN_SECRET_KEY" -p "$MINISIGN_PUBLIC_KEY"
  fi

  mkdir -p "${AGE_USERS}/hooks"
  cp "${examples_dir}/hooks/add-users_GitHub" "${AGE_USERS}/hooks/"
  cp "${examples_dir}/hooks/guess-default-recipients" "${AGE_USERS}/hooks/"

  mkdir -p "${AGE_DIR}/hooks"
  cp "${examples_dir}/hooks/ensure-identities" "${AGE_DIR}/hooks/"

  echo "$0" > small_file
  date >> small_file
  assert [ -s small_file ]
}


canned_setup() {
  export HOME=$(TMPDIR=${BATS_FILE_TMPDIR} mktemp -d)
  export XDG_DATA_HOME="$HOME/.local/share"
  eval $( $target paths )

  echo "Replacing ${examples_dir}/example-encryption-key -> $AGE_SECRET_KEY"
  install -D "${examples_dir}/example-encryption-key" "$AGE_SECRET_KEY"
  echo "Replacing ${examples_dir}/example-signing-key.pub -> $MINISIGN_USERS"
  install -D "${examples_dir}/example-signing-key.pub" "$MINISIGN_USERS"
}

