.PHONY: test@bash coverage@python lint publish push test

package_name = frink
source_dir = ${package_name}

# https://github.com/nalgeon/podsearcg-py/blob/main/Makefile
# via
# https://antonz.org/python-packaging/

test : test@bash coverage@python

test@bash :
	mkdir -p results/bats
	# Tests require input:
	[ -t 0 ] && tests/run-bats tests \
		--report-formatter junit -o results/bats

coverage@python :
	python -m coverage erase
	mkdir -p coverage/pytest
	# Tests require input: (use pytest -ra otherwise)
	[ -t 0 ] && python -m coverage run -a --omit='tests/lib/*,.tox/*,venv/*' -m pytest -s \
		--junit-xml=results/pytest/report.xml
	python -m coverage html -d coverage/pytest

lint :
	python -m pylama

publish:  ## Publish to PyPi
	python -m flit publish

push:  ## Push code with tags
	git push && git push --tags

